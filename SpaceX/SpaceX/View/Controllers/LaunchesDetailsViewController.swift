//
//  LaunchesDetailsViewController.swift
//  SpaceX
//
//  Created by Naem on 01/28/2020.
//  Copyright © 2020 Naem. All rights reserved.
//

import UIKit

class LaunchesDetailsViewController: UIViewController {

    @IBOutlet weak var launchesDetailsTableView: UITableView!
    
    var viewModel: LaunchesDetailsViewModelConfigurable! {
        didSet {
            viewModel.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Details"        
        launchesDetailsTableView.delegate = self
        launchesDetailsTableView.dataSource = self
        launchesDetailsTableView.rowHeight = UITableView.automaticDimension
        launchesDetailsTableView.estimatedRowHeight = 44.0
        launchesDetailsTableView.register(CenterImageTableViewCell.self)
        launchesDetailsTableView.register(TitleAndValueTableViewCell.self)
    }
}

//MARK: TableViewDelegate and TableViewDataSource

extension LaunchesDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsPerSection(forSection: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 200 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = LaunchDetailsSections(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        let vm = viewModel.cellViewModel(forIndexPath: indexPath)
        
        switch section {
        case .launchImage:
             let imageCell:CenterImageTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            imageCell.viewModel = vm
            return imageCell
        case .launchData:
             let titleAndValueCell:TitleAndValueTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            titleAndValueCell.viewModel = vm
            return titleAndValueCell
        }
    }
}

extension LaunchesDetailsViewController: LaunchesDetailsViewModelDelegate {
    func reloadData() {
        launchesDetailsTableView.reloadData()
    }
}
