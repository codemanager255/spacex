//
//  HistroicalLunchesCellViewModel.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

struct HistroicalLunchesCellViewModel: BasicTableViewCellConfigurable {
    var spaceXMission: SpaceXMission?
  
    init(mission: SpaceXMission?) {
        self.spaceXMission = mission
    }
    var headerLabelText: String? {
        return spaceXMission?.missionName
    }
    var stackLabel1Text: String? {
        return "" //(spaceXMission?.rocket.rocketName).map { $0.rawValue }
    }
    
    var stackLabel2Text: String? {
        return "" //(spaceXMission?.launchSite.siteName).map { $0.rawValue }
    }
    var stackLabel3Text: String? {
        return spaceXMission?.launchDateUTC
    }
    var cellDefaultName: String {
        return "spaceXLogo"
    }
    var imageUrl: String? {
        return ""//spaceXMission?.links.missionPatchSmall
    }
}
