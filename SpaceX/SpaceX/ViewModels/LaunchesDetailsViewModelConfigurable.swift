//
//  LunchesDetailsViewModelConfigurable.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//

import Foundation

public protocol LaunchesDetailsViewModelConfigurable: TableViewDelegateAndDataSourceConfigurable {
    var delegate: LaunchesDetailsViewModelDelegate? { get set }
}
