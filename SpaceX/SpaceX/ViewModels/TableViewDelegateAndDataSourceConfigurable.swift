//
//  TableViewDelegateAndDataSourceConfigurable.swift
//  SpaceX
//
//  Created by Mahesh on 01/28/2020.
//  Copyright © 2020 Mahesh. All rights reserved.
//
import Foundation

public protocol TableViewDelegateAndDataSourceConfigurable {
    func numberOfSections() -> Int
    func cellViewModel(forIndexPath indexPath: IndexPath) -> CellViewModelConfigurable
    func rowsPerSection(forSection section: Int) -> Int
}
